# wait-for-nomad

Утилита для ожидания окончания деплоя nomad'а. Постоянно опрашивает номад о состоянии указанной джобы. Отдает exit code 1, если деплоймент падает и 0, если заканчивается успешно. В процессе выводит информацию о группах и аллокациях.

```
> docker run --env JOB=link-old-prod --env NOMAD_ADDR=${NOMAD_ADDR}  registry.gitlab.com/s.korchagin/wait-for-nomad:latest
time="2020-07-30T13:55:59Z" level=info msg="Found a deployment `edd085bd-a99e-6a87-8688-499098b80ff9` with status `successful`"
time="2020-07-30T13:55:59Z" level=info msg="Deployment successful"
```

