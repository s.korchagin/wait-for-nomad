module sftype/wait-for-nomad

go 1.14

require (
	github.com/hashicorp/nomad/api v0.0.0-20200729144347-d245908718ce
	github.com/sirupsen/logrus v1.6.0
)
