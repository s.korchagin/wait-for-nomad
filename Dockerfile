FROM golang:1.13.7-alpine3.11 as build
ENV \
    TERM="xterm-color" \
    TIME_ZONE="UTC" \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GOFLAGS="-mod=vendor" \
    GOLANGCI_VERSION="v1.27.0"
RUN \
    echo "## Prepare timezone" && \
    apk add --no-cache --update tzdata && \
    cp /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime && \
    echo "${TIME_ZONE}" > /etc/timezone && date
RUN \
    echo "## Install golangci" && \
    wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin ${GOLANGCI_VERSION} && \
    golangci-lint --version

WORKDIR /app
ADD ./go.* ./
ADD ./*.go ./
ADD ./vendor ./vendor
ADD .golangci.yml .golangci.yml

RUN golangci-lint run --timeout 3m

RUN go build -o app .

#######################
FROM scratch

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/localtime /etc/localtime
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group

COPY --from=build /app/app /app/app

USER nobody:nobody

WORKDIR /app

CMD ["./app"]
